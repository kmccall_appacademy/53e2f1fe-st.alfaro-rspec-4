class Temperature
  F_C_PROPORTION = 5.to_f / 9 # or use 5.0/9 to force floating-point
  F_C_FREEZE_DELTA = 32

  def initialize(val)
    @instance_type = construct_instance_type(val)
  end

  def in_fahrenheit
    @instance_type.in_fahrenheit
  end

  def in_celsius
    @instance_type.in_celsius
  end

  def self.from_celsius(temperature)
    Celsius.new(temperature)
  end

  def self.from_fahrenheit(temperature)
    Fahrenheit.new(temperature)
  end

  private

  def construct_instance_type(temp_val)
    if temp_val.include?(:f)
      Temperature.from_fahrenheit(temp_val[:f])
    else
      Temperature.from_celsius(temp_val[:c])
    end
  end

  def ftoc(f_temp)
    (f_temp - F_C_FREEZE_DELTA) * F_C_PROPORTION
  end

  def ctof(c_temp)
    (c_temp * 1 / F_C_PROPORTION) + F_C_FREEZE_DELTA
  end

end

class Celsius < Temperature
  def initialize(val)
    @value = val
  end

  def in_fahrenheit
    ctof(@value)
  end

  def in_celsius
    @value
  end

end

class Fahrenheit < Temperature
  def initialize(val)
    @value = val
  end

  def in_fahrenheit
    @value
  end

  def in_celsius
    ftoc(@value)
  end

end
