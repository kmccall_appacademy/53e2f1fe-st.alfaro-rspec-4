class Book
  DO_NOT_CAPITALIZE = [:the, :and, :a, :an, :of, :in]
  @title

  def title
    @title
  end

  def title=(value)
    @title = capitalize(value)
  end

  private

  def capitalize(value)
    words = value.split(" ")
    capitalize_first = true
    words.map! do |word|
      if capitalize_first || !DO_NOT_CAPITALIZE.include?(word.to_sym)
        capitalize_first = false
        word.capitalize
      else
        word
      end
    end
    words.join(" ")
  end

end
