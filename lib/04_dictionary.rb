class Dictionary
  attr_reader :entries
  def initialize
    @entries = {}
  end

  def add(entry)
    new_entry = entry.is_a?(Hash) ? entry : { entry => nil }
    @entries.merge!(new_entry)
  end

  def keywords
    @entries.keys.sort
  end

  def include?(key)
    @entries.include?(key)
  end

  def find(key)
    regexp = Regexp.new(key.to_s)
    @entries.select do |k, _v|
      k =~ regexp
    end
  end

  def printable
    is_first = true
    output = String.new
    keywords.each do |k|
      output << "\n" unless is_first
      output << "[#{k}] \"#{@entries[k]}\""
      is_first = false
    end
    output
  end

end
