class Friend
  def greeting(who = nil)
    greeting = "Hello"
    if who
      greeting << ", #{who}"
    end
    greeting << "!"
  end
end
