require 'byebug'
class Timer
  attr_accessor :seconds
  def initialize
    @seconds = 0
  end

  def time_string
    time = []
    total_seconds = @seconds
    2.downto(0) do |idx|
      time_val = 0
      divisor = 60**idx
      if total_seconds >= divisor
        time_val = total_seconds / divisor
        total_seconds -= divisor*time_val
      end
      time << padded(time_val)
    end
    time.join(":")
  end

  def padded(value)
    string_val = value.to_s
    if value < 10
      string_val = "0#{value}"
    end
    string_val
  end

end
